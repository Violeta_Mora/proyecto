import pika

url = 'amqps://hcdcjyrp:34oW03OIkQjGfLtJF6uyK3W_YqgZA605@orangutan.rmq.cloudamqp.com/hcdcjyrp'
params = pika.URLParameters(url)
connection = pika.BlockingConnection(params)
channel = connection.channel() # start a channel

channel.queue_declare(queue='Humedad del aire') # Declare a queue
for i in range(50):
                channel.basic_publish(exchange='', routing_key='Humedad del aire', body='Humedad aceptable entre 40% y 70%')


channel.queue_declare(queue='Humedad del suelo') # Declare a queue
for i in range(50):
                channel.basic_publish(exchange='', routing_key='Humedad del suelo', body='Capacidad de retención entre 30% y 60%' )

channel.queue_declare(queue='Temperatura') # Declare a queue
for i in range(50):
                channel.basic_publish(exchange='', routing_key='Temperatura', body='Por encima de 5º C para la germinación' )

channel.queue_declare(queue='Presión del agua') # Declare a queue
for i in range(50):
                channel.basic_publish(exchange='', routing_key='Presión del agua', body='Caudal de de agua de 8 l/h y presión de 5 a 40 m.c.a' )

channel.queue_declare(queue='Indice UV') # Declare a queue
for i in range(50):
                channel.basic_publish(exchange='', routing_key='Indice UV', body='Indice UV moderado de 3 a 5')


connection.close()