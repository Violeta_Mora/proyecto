import pika

url = 'amqps://hcdcjyrp:34oW03OIkQjGfLtJF6uyK3W_YqgZA605@orangutan.rmq.cloudamqp.com/hcdcjyrp'
params = pika.URLParameters(url)
connection = pika.BlockingConnection(params)
channel = connection.channel() # start a channel

channel.queue_declare(queue='Humedad del aire') # Declare a queue
def callback(ch, method, properties, body):
  print(" [x] Recibido " + str(body))
channel.basic_consume('Humedad del aire', callback, auto_ack=True)
print(' [*] Waiting for messages: Humedad')

channel.queue_declare(queue='Humedad del suelo') # Declare a queue
def callback(ch, method, properties, body):
  print(" [x] Recibido " + str(body))
channel.basic_consume('Humedad del suelo', callback, auto_ack=True)
print(' [*] Waiting for messages: Humedad')

channel.queue_declare(queue='Temperatura') # Declare a queue
def callback(ch, method, properties, body):
  print(" [x] Recibido " + str(body))
channel.basic_consume('Temperatura', callback, auto_ack=True)
print(' [*] Waiting for messages: Temperatura')

channel.queue_declare(queue='Presión del agua') # Declare a queue
def callback(ch, method, properties, body):
  print(" [x] Recibido " + str(body))
channel.basic_consume('Presión del agua', callback, auto_ack=True)
print(' [*] Waiting for messages: Agua')

channel.queue_declare(queue='Indice UV') # Declare a queue
def callback(ch, method, properties, body):
  print(" [x] Recibido " + str(body))
channel.basic_consume('Indice UV', callback, auto_ack=True)
print(' [*] Waiting for messages: Indice UV')


channel.start_consuming()
connection.close()